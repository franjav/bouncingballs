function build_sphere(rings, segments, radius, baseColor)
{
	var vertexPositionData = [];
    var normalData = [];
    var textureCoordData = [];
    var colorData = [];

    for (var ringsNumber = 0; ringsNumber <= rings; ringsNumber++) 
    {
        var theta = ringsNumber * Math.PI / rings;
        var sinTheta = Math.sin(theta);
        var cosTheta = Math.cos(theta);

        for (var segmentsNumber = 0; segmentsNumber <= segments; segmentsNumber++) 
        {
            var phi = segmentsNumber * 2 * Math.PI / segments;
            var sinPhi = Math.sin(phi);
            var cosPhi = Math.cos(phi);
            
            var x = cosPhi * sinTheta;
            var y = cosTheta;
            var z = sinPhi * sinTheta;
            var u = 1 - (segmentsNumber / segments);
            var v = 1 - (ringsNumber / rings);

            normalData.push(x);
            normalData.push(y);
            normalData.push(z);

            textureCoordData.push(u);
            textureCoordData.push(v);

            vertexPositionData.push(radius * x);
            vertexPositionData.push(radius * y);
            vertexPositionData.push(radius * z);

            colorData.push(baseColor[0]);
            colorData.push(baseColor[1]);
            colorData.push(baseColor[2]);
            colorData.push(baseColor[3]);
        }
    }
    var indexData = [];
    for (var ringsNumber = 0; ringsNumber < rings; ringsNumber++) 
    {
        for (var segmentsNumber = 0; segmentsNumber < segments; segmentsNumber++) 
        {
            var first = (ringsNumber * (segments + 1)) + segmentsNumber;
            var second = first + segments + 1;

            indexData.push(first);
            indexData.push(second);
            indexData.push(first + 1);

            indexData.push(second);
            indexData.push(second + 1);
            indexData.push(first + 1);
        }
    }

	return [vertexPositionData, normalData, textureCoordData, colorData, indexData];
}
