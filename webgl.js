
//WebGL variables
var canvas;
var gl;


//Window
var WindowWidth, WindowHeight, ncp = 0.1, fcp = 20;

var perspectiveMatrix;


var sphereData, sphereBuffer, sphereBufferNormal, sphereBufferIndex;


var boxData, boxBuffer, boxBufferNormal, boxBufferIndex;


var shaderProgram;

var Step = 50;

var SphereVec = [];

var gravity;

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just have
// one object -- a simple two-dimensional square.
//
function initBuffers() {
  //Generate the sphere buffer 
  sphereData = build_sphere(20, 20, 1.0, [1.0, 1.0, 1.0, 1.0]);
  sphereBuffer = gl.createBuffer();
  sphereBufferNormal = gl.createBuffer();
  sphereBufferIndex = gl.createBuffer();
  

  sphereBuffer.numItems = sphereData[0].length;
  sphereBuffer.itemSize = 3;

  sphereBufferIndex.numItems = sphereData[4].length;
  sphereBufferIndex.itemSize = 1;

  sphereBufferNormal.numItems = sphereData[1].length;
  sphereBufferNormal.itemSize = 3;

  gl.bindBuffer(gl.ARRAY_BUFFER, sphereBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereData[0]), gl.STATIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER, sphereBufferNormal);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereData[1]), gl.STATIC_DRAW);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphereBufferIndex);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(sphereData[4]), gl.STATIC_DRAW);


  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);


  //creating the box
  boxData = [];
  boxBuffer = gl.createBuffer();
  boxBufferNormal = gl.createBuffer();
  boxBufferIndex = gl.createBuffer();
  

  boxData = [
            //vertex
            [ //bot
              -1,-1,1, 1,-1,1,1,-1,-1,-1,-1,-1,

              //top
              -1,1,1, 1,1,1, 1,1,-1,-1,1,-1,

              //left
              -1,-1,1,-1,-1,-1,-1,1,-1,-1,1,1,

              //right
              1,-1,1, 1,1,1, 1,1,-1, 1,-1,-1,

              //back
              -1,-1,-1, 1,-1,-1, 1,1,-1, -1,1,-1
              ],

              //normals
            [ 0,1,0,0,1,0,0,1,0,0,1,0,

              0,-1,0,0,-1,0,0,-1,0, 0,-1,0,

              -1,0,0,-1,0,0,-1,0,0, -1,0,0,

              1,0,0,1,0,0,1,0,0, 1,0,0,

              0,0,1,0,0,1,0,0,1, 0,0,1,
            ],
            
            //index
            [ 0,1,2,0,2,3,

              4,5,6,4,6,7,

              8,9,10,8,10,11,

              12,13,14,12,14,15, 

              16,17,18,16,18,19, 
              ]
              ];

  boxBuffer.numItems = boxData[0].length;
  boxBuffer.itemSize = 3;

  boxBufferNormal.numItems = boxData[1].length;
  boxBufferNormal.itemSize = 3;

  boxBufferIndex.numItems = boxData[2].length;
  boxBufferIndex.itemSize = 1;

  gl.bindBuffer(gl.ARRAY_BUFFER, boxBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxData[0]), gl.STATIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER, boxBufferNormal);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxData[1]), gl.STATIC_DRAW);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxBufferIndex);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxData[2]), gl.STATIC_DRAW);


  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

//
// start
//
// Called when the canvas is created to get the ball rolling.
// Figuratively, that is. There's nothing moving in this demo.
//
function start() {
	

  canvas = document.getElementById("glcanvas");
  canvas.addEventListener("webglcontextlost", function(event) {
            event.preventDefault();
          }, false);


      
  WindowWidth = canvas.width;
  WindowHeight = canvas.height;
  


  initWebGL(canvas);      // Initialize the GL context


  // Only continue if WebGL is available and working
  
  if (gl) {
  
    gl.viewport(0,0,WindowWidth,WindowHeight);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
    
    // Initialize the shaders; this is where all the lighting for the
    // vertices and so forth is established.
    //initShaders();

    
    //Make the perspective Matrix
    perspectiveMatrix = makePerspective(45.0, WindowWidth/WindowHeight, ncp, fcp);
     

    gravity = $V([0,-0.03,0])

    initBuffers();

    initShaders();


    add();
	
    startTime = Date.now();

    
    // Set up to draw the scene periodically.
    setInterval(drawScene, 1);
  }else{
    console.log("Your browser doesn't support WebGL");
  }
}

//
// initWebGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initWebGL() {
  gl = null;

  
  try {
    gl = canvas.getContext("experimental-webgl");
  }
  catch(e) {
  }
  
  // If we don't have a GL context, give up now
  
  if (!gl) {
    alert("Unable to initialize WebGL. Your browser may not support it.");
  }
}



//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
  var fragmentShader = getShader(gl, "shader-fs");
  var vertexShader = getShader(gl, "shader-vs");
  
  // Create the shader program
  
  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  
  // If creating the shader program failed, alert
  
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert("Unable to initialize the shader program.");
  }
  
  gl.useProgram(shaderProgram);
  
  vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPos");
  gl.enableVertexAttribArray(vertexPositionAttribute);

  normalPositionAttribute = gl.getAttribLocation(shaderProgram, "aNormalPosition");
  gl.enableVertexAttribArray(normalPositionAttribute);
}



//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
  var shaderScript = document.getElementById(id);
  
  // Didn't find an element with the specified ID; abort.
  
  if (!shaderScript) {
    return null;
  }
  
  // Walk through the source element's children, building the
  // shader source string.
  
  var theSource = "";
  var currentChild = shaderScript.firstChild;
  
  while(currentChild) {
    if (currentChild.nodeType == 3) {
      theSource += currentChild.textContent;
    }
    
    currentChild = currentChild.nextSibling;
  }
  
  // Now figure out what type of shader script we have,
  // based on its MIME type.
  
  var shader;
  
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;  // Unknown shader type
  }
  
  // Send the source to the shader object
  
  gl.shaderSource(shader, theSource);
  
  // Compile the shader program
  
  gl.compileShader(shader);
  
  // See if it compiled successfully
  
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
    return null;
  }
  
  return shader;
}



//
// drawScene
//
// Draw the scene.
//
function drawScene() {
  

   // Clear the canvas before we start drawing on it.
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.useProgram(shaderProgram);

  //Increment the rotation of planet and moon
  now = Date.now();
  elapsed = now - startTime;
  if(elapsed >= Step){
    update(elapsed);
    startTime = now;
  }

  //Load identity to Model View Matrix
  let pUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  gl.uniformMatrix4fv(pUniform, false, new Float32Array(perspectiveMatrix.flatten()));

  
  gl.bindBuffer(gl.ARRAY_BUFFER, sphereBuffer);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphereBufferIndex);

  gl.enableVertexAttribArray(vertexPositionAttribute);
  gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  gl.bindBuffer(gl.ARRAY_BUFFER, sphereBufferNormal);
  
  gl.enableVertexAttribArray(normalPositionAttribute);
  gl.vertexAttribPointer(normalPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  
  let modelUniform = gl.getUniformLocation(shaderProgram, "uModelMatrix");
  let viewUniform = gl.getUniformLocation(shaderProgram, "uViewMatrix");
  let colorUniform = gl.getUniformLocation(shaderProgram, "SphereColor");


  let uViewMatrix = Matrix.glTranslate($V([0.0, 0.0, -4]));
  gl.uniformMatrix4fv(viewUniform, false, new Float32Array(uViewMatrix.flatten()));

      //Draw spheres
  for(let i=0;i<SphereVec.length;++i){
    //translate to its position
    let uModelMatrix = Matrix.glTranslate(SphereVec[i].position);
    //scale by its size
    let scale = Matrix.glScale($V([SphereVec[i].radius, SphereVec[i].radius, SphereVec[i].radius]));
    //set the color
    gl.uniform3fv(colorUniform, new Float32Array(SphereVec[i].color));


    uModelMatrix = uModelMatrix.x(scale);


    gl.uniformMatrix4fv(modelUniform, false, new Float32Array(uModelMatrix.flatten()));


    //draw the sphere
    gl.drawElements(gl.TRIANGLES, sphereBufferIndex.numItems, gl.UNSIGNED_SHORT, 0);
  }
  

  //now draw the plane
  gl.bindBuffer(gl.ARRAY_BUFFER, boxBuffer);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxBufferIndex);

  gl.enableVertexAttribArray(vertexPositionAttribute);
  gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  gl.bindBuffer(gl.ARRAY_BUFFER, boxBufferNormal);
  
  gl.enableVertexAttribArray(normalPositionAttribute);
  gl.vertexAttribPointer(normalPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  uModelMatrix = Matrix.glTranslate($V([0.0, 0.0, 0.0]));
  gl.uniformMatrix4fv(modelUniform, false, new Float32Array(uModelMatrix.flatten()));

  gl.uniform3fv(colorUniform, new Float32Array([1.0,1.0,1.0]));

  //draw the plane
  gl.drawElements(gl.TRIANGLES, boxBufferIndex.numItems, gl.UNSIGNED_SHORT, 0);
}


//reset the sphere vector
function reset(){
  SphereVec = [];
}

//add as many spheres as the user wants
function add(){
  let howMany = document.getElementById("quantity").value;

  let max = document.getElementById("quantity").max;

  //only allow 20 spheres to be added at the same time
  if(howMany > max) max = howMany;

  //don't let then be more than 150
  if(SphereVec.length > 150) return;

  //add spheres

  for(let i=0;i<parseInt(howMany);++i){
    SphereVec.push({
      position: $V([Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1]), //position in the position [-1,1]^3
      radius: Math.random()/8+0.04,
      color: [Math.random(), Math.random(), Math.random()], //color in [0,1]^3
      velocity:  $V([0,0,0]),
      acceleration: $V([Math.random()/4, Math.random()/4, Math.random()/4]), //initial acceleration in [0,1]^3
    });
  }

  
}



function applyForce(i, force){

  SphereVec[i].acceleration = SphereVec[i].acceleration.add(force);

}



function updateBallPosition(i){
  //modify velocity by acceleration
  SphereVec[i].velocity = SphereVec[i].velocity.add(SphereVec[i].acceleration);


  //modify position by the velocity
  SphereVec[i].position = SphereVec[i].position.add(SphereVec[i].velocity);

  //acceleration is reseted
  SphereVec[i].acceleration.elements[0] = 0.0;
  SphereVec[i].acceleration.elements[1] = 0.0;
  SphereVec[i].acceleration.elements[2] = 0.0;
}


//check the ball position to avoid going out of boundries 
function correctEdges(i){

  //check every component
  for(let j=0;j<3;++j){
    if (SphereVec[i].position.elements[j] + SphereVec[i].radius > 1) {
      SphereVec[i].position.elements[j] = 1 - SphereVec[i].radius;
      SphereVec[i].velocity.elements[j] *= -1;
    } else if (SphereVec[i].position.elements[j] - SphereVec[i].radius < -1) {
      SphereVec[i].position.elements[j] = -1 + SphereVec[i].radius;
      SphereVec[i].velocity.elements[j] *= -1;
    }
  }
}



//function to update the position of the spheres 
function update(elapsed){

  //calculate friction
  for(let i=0;i<SphereVec.length;++i){
    
    var c = 0.01;
    var normal = 1;
    var frictionMag = c * normal;
    var friction = $V(SphereVec[i].velocity);

    //to the 
    friction = friction.multiply(-1);
    friction = friction.toUnitVector();
    friction = friction.multiply(frictionMag);



    //apply friction
    applyForce(i, friction);

    //apply force of gravity
    applyForce(i, gravity);

    updateBallPosition(i);

    correctEdges(i);
  }

}
