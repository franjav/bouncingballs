# WebGL Bouncing Balls

This is a simple example of a software with bouncing balls physics. 

## External libraries

The following externa libraries/ projects were used in the develop of this program:

* [Sylvester.js 0.1.3](http://sylvester.jcoglan.com/), for matrix and vector manipulation

## Screenshots

The following is a screenshot of the program usage:

<p align="center">
  <img src ="./ScreenShot/render1.png" />
</p>
